//
//  homework.swift
//  Landmarks
//
//  Created by student on 1/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import SwiftUI

struct homework: View {
    var landmark: Landmark

    var body: some View {
        VStack {
            MapView(coordinate: landmark.locationCoordinate)
                .edgesIgnoringSafeArea(.top)
                .frame(height: 300)

            CircleImage(image: landmark.image)
                .offset(x: 0, y: -130)
                .padding(.bottom, -130)

            VStack(alignment: .leading) {
                Text("Jakkarin")
                    .font(.title)

                HStack(alignment: .top) {
                    Text(landmark.park)
                        .font(.subheadline)
                    Spacer()
                    Text(landmark.state)
                        .font(.subheadline)
                }
            }
            .padding()

            Spacer()
        }
        .navigationBarTitle(Text(verbatim: landmark.name), displayMode: .inline)
    }
}

struct homework_Previews: PreviewProvider {
    static var previews: some View {
        homework(landmark: landmarkData[0])
    }
}
