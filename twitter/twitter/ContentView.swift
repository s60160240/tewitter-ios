//
//  ContentView.swift
//  twitter
//
//  Created by student on 1/11/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack{
        VStack(alignment: .leading){
            Image("twitter-logo")
            .resizable()
            .edgesIgnoringSafeArea(.top)
            .frame(height:160)
            VStack(alignment: .leading){
                Text("Chonphisit")
                    .font(.headline)
                Text("@bew_256")
                Text("เข้าร่วมเมื่อ ตุลาคม 2562")
                Text("0 กำลังติดตาม 0 ผู้ติดตาม")
            }
        .padding()
            .frame(height:160)
        }
        .frame(height:320.0)
        
        Image("ichigo")
            .resizable().frame(width:75.0,height:75.0)
            .clipShape(Circle()).overlay(Circle().stroke(Color.white,lineWidth: 2)).shadow(radius:10)
            .offset(x:-240,y:10)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().previewLayout(.fixed(width: 568, height: 320))
    }
}
