//
//  Supporting.swift
//  twitter
//
//  Created by student on 1/11/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI

struct Supporting: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct Supporting_Previews: PreviewProvider {
    static var previews: some View {
        Supporting()
    }
}
